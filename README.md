This repository contains some webpages I run on the T3. It is hacked-together code from two sources:

https://github.com/BlackrockDigital/startbootstrap-sb-admin-2

https://www.evoluted.net/thinktank/web-development/php-directory-listing-script

as well as many jquery packages. In particular, I make heavy use of Flot and Flot plugins. 
